import math

start = 312051

# get closest odd square root, this is the length of a side
# (bottom right corners is the sequence of odd squares)
root = math.ceil(math.sqrt(start))
side_length = root if root % 2 != 0 else root + 1

# steps from side of length N is the ordinal of N in the sequence of odd numbers
axis_to_center = (side_length - 1) // 2

# get smallest amount of steps from input to one of the four axes
start_to_axis = min(list(map(lambda x: abs(x - start), [side_length**2 - \
                ((side_length - 1) * axis)  - math.floor(side_length // 2) \
                for axis in range(0, 4)])))

print(start_to_axis + axis_to_center)

# answer to part 2: https://oeis.org/A141481/b141481.txt
