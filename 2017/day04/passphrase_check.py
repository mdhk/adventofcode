from itertools import combinations

passphrases = [line.split(' ') for line in open('passphrases.txt', 'r').read().splitlines()]

phrases_without_duplicates = 0
phrases_without_anagrams = 0

for passphrase in passphrases:
    if len(passphrase) == len(set(passphrase)):
        phrases_without_duplicates += 1
    if all(sorted(word_1) != sorted(word_2) for word_1, word_2 in combinations(passphrase, 2)):
        phrases_without_anagrams += 1

print(phrases_without_duplicates, phrases_without_anagrams)
