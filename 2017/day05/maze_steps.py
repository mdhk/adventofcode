offsets = [int(l.strip()) for l in open('jumps.txt', 'r').readlines()]

def get_out_of_maze(offsets, increase_limit=None):
    index, steps = 0, 0
    while -1 < index < len(offsets):
        offset = offsets[index]
        if increase_limit is not None and offset > increase_limit:
            offsets[index] -= 1
        else:
            offsets[index] += 1
        index = index + offset
        steps += 1
    return(steps)

print(get_out_of_maze(offsets[:]), get_out_of_maze(offsets[:], increase_limit=2))
