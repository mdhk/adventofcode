digit_list = [int(i) for i in list(open('digits.txt', 'r').read().strip())]
half_circle = int(len(digit_list) / 2)

def captcha_sum(digit_list, step_size=1):
    return sum([d for i, d in enumerate(digit_list)
                if d == digit_list[(i + step_size) % len(digit_list)]])

print(captcha_sum(digit_list))
print(captcha_sum(digit_list, step_size=half_circle))

# # complete oneliner
# print((lambda digit_list: sum([d for i, d in enumerate(digit_list) \
#                 if d == digit_list[(i + int(len(digit_list)/2)) % len(digit_list)]])) \
#                 ([int(i) for i in list(open('digits.txt', 'r').read().strip())]))
