from itertools import combinations

spreadsheet = [list(map(int, row.split('\t'))) for row in \
                open('spreadsheet.txt', 'r').read().splitlines()]

def checksum(spreadsheet):
    return sum(max(row) - min(row) for row in spreadsheet)

def evenly_divisible_sum(spreadsheet):
    return sum([j // i for row in spreadsheet for i, j in \
            combinations(sorted(row), 2) if j % i == 0])

print(checksum(spreadsheet))
print(evenly_divisible_sum(spreadsheet))
